/*!
 * project_shg
 * 
 * 
 * @author Thuclfc
 * @version 2.0.0
 * Copyright 2024. MIT licensed.
 *//*!
 *
 *
 * @author Thuclfc
 * @version
 * Copyright 2020. MIT licensed.
 */
"use strict";

var prev = $('.prev');
var next = $('.next');
var prev1 = $('.prev1');
var next1 = $('.next1');
var inner = $('.month_year');
var inner1 = $('.month_year1');
var datetime = $('.cld_body_start');
var datetime1 = $('.cld_body_end');
var time = '';
var start_date;
var start_month;
var start_year;
var tmp_start_month;
var tmp_start_year;
var end_date;
var end_month;
var end_year;
var tmp_end_month;
var tmp_end_year;
function renderCalendar(month, year) {
  var c_month = month;
  var c_year = year;
  var html = "";
  var day_of_month = 0;
  var flagy = false; //false la ko ket thuc = 00; true la ket thuc bang 00

  var flagm = false; //false la sai thang, true la dung thang

  var nhuan = false; // false la ko nhuan, true la nhuan

  var dowen = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
  if (c_year.length == 4 && c_year.toString().substring(0, 2) != "00") {
    flagy = false;
    if (parseInt(c_year) % 4 == 0) {
      nhuan = true;
    } else {
      nhuan = false;
    }
  } else {
    flagy = true;
    if (parseInt(c_year.toString().substring(0, 2)) % 4 == 0) {
      nhuan = true;
    } else {
      nhuan = false;
    }
  }
  if (c_month > 0 && c_month <= 12) {
    flagm = true;
  } else {
    flagm = false;
  }
  switch (c_month) {
    case 1:
    case 3:
    case 5:
    case 7:
    case 8:
    case 10:
    case 12:
      day_of_month = 31;
      break;
    case 2:
      if (nhuan) {
        day_of_month = 29;
      } else {
        day_of_month = 28;
      }
      break;
    case 4:
    case 6:
    case 9:
    case 11:
      day_of_month = 30;
      break;
  }
  if (flagm) {
    var li = "";
    for (var i = 1; i <= day_of_month; i++) {
      if (i == 1) {
        var datetmp = new Date(c_year + "-" + c_month + "-" + i);
        var num_start = datetmp.getDay();
        if (num_start == 0) {
          for (var j = 0; j < num_start; j++) {
            li += "<li></li>";
          }
        } else {
          for (var j = 0; j < num_start; j++) {
            li += "<li></li>";
          }
        }
      }
      var classActive = '';
      var start_datepick = '';
      var range = '';
      if (start_year < c_year && c_year < end_year) {
        range = 'in_range';
      } else if (start_year == c_year && c_year < end_year) {
        if (start_month < c_month) {
          range = 'in_range';
        } else if (start_month == c_month) {
          if (start_date < i) {
            range = 'in_range';
          }
        }
      } else if (start_year < c_year && c_year == end_year) {
        if (c_month < end_month) {
          range = 'in_range';
        } else if (c_month == end_month) {
          if (end_date >= i) {
            range = 'in_range';
          }
        }
      } else {
        if (start_month < c_month && c_month < end_month) {
          range = 'in_range';
        } else if (start_month == c_month && c_month < end_month) {
          if (start_date < i) {
            range = 'in_range';
          }
        } else if (start_month < c_month && c_month == end_month) {
          if (end_date >= i) {
            range = 'in_range';
          }
        } else if (start_month == c_month && c_month == end_month) {
          if (start_date < i && end_date >= i) {
            range = 'in_range';
          }
        }
      }
      if (start_date == i && start_month == month && start_year == year) {
        classActive = 'active';
        start_datepick = 'starday';
      }
      li += '<li class="day day_' + i + ' ' + classActive + '"><span data-time="' + i + '" class="time ' + start_datepick + ' ' + range + '">' + i + '</span></li>';
      if (i == day_of_month) {
        datetmp = new Date(c_year + "-" + c_month + "-" + i);
        num_start = datetmp.getDay();
        for (j = 0; j < 6 - num_start; j++) {
          li += "<li></li>";
        }
      }
    }
    datetime.html(li);
  }
}
function renderCalendar1(month, year) {
  var c_month = month;
  var c_year = year;
  var html = "";
  var day_of_month = 0;
  var flagy = false; //false la ko ket thuc = 00; true la ket thuc bang 00

  var flagm = false; //false la sai thang, true la dung thang

  var nhuan = false; // false la ko nhuan, true la nhuan

  var dowen = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
  if (c_year.length == 4 && c_year.toString().substring(0, 2) != "00") {
    flagy = false;
    if (parseInt(c_year) % 4 == 0) {
      nhuan = true;
    } else {
      nhuan = false;
    }
  } else {
    flagy = true;
    if (parseInt(c_year.toString().substring(0, 2)) % 4 == 0) {
      nhuan = true;
    } else {
      nhuan = false;
    }
  }
  if (c_month > 0 && c_month <= 12) {
    flagm = true;
  } else {
    flagm = false;
  }
  switch (c_month) {
    case 1:
    case 3:
    case 5:
    case 7:
    case 8:
    case 10:
    case 12:
      day_of_month = 31;
      break;
    case 2:
      if (nhuan) {
        day_of_month = 29;
      } else {
        day_of_month = 28;
      }
      break;
    case 4:
    case 6:
    case 9:
    case 11:
      day_of_month = 30;
      break;
  }
  if (flagm) {
    var li = "";
    for (var i = 1; i <= day_of_month; i++) {
      if (i == 1) {
        var datetmp = new Date(c_year + "-" + c_month + "-" + i);
        var num_start = datetmp.getDay();
        if (num_start == 0) {
          for (var j = 0; j < num_start; j++) {
            li += "<li></li>";
          }
        } else {
          for (var j = 0; j < num_start; j++) {
            li += "<li></li>";
          }
        }
      }
      var classActive = '';
      var start_datepick = '';
      var range = '';
      if (start_year < c_year && c_year < end_year) {
        range = 'in_range';
      } else if (start_year == c_year && c_year < end_year) {
        if (start_month < c_month) {
          range = 'in_range';
        } else if (start_month == c_month) {
          if (start_date <= i) {
            range = 'in_range';
          }
        }
      } else if (start_year < c_year && c_year == end_year) {
        if (c_month < end_month) {
          range = 'in_range';
        } else if (c_month == end_month) {
          if (end_date >= i) {
            range = 'in_range';
          }
        }
      } else {
        if (start_month < c_month && c_month < end_month) {
          range = 'in_range';
        } else if (start_month == c_month && c_month < end_month) {
          if (start_date <= i) {
            range = 'in_range';
          }
        } else if (start_month < c_month && c_month == end_month) {
          if (end_date >= i) {
            range = 'in_range';
          }
        } else if (start_month == c_month && c_month == end_month) {
          if (start_date <= i && end_date >= i) {
            range = 'in_range';
          }
        }
      }
      if (end_date == i && month == end_month && year == end_year) {
        classActive = 'active';
        start_datepick = 'endday';
      }
      li += '<li class="day day_' + i + ' ' + classActive + '"><span data-time="' + i + '" class="time ' + start_datepick + ' ' + range + '">' + i + '</span></li>';
      if (i == day_of_month) {
        datetmp = new Date(c_year + "-" + c_month + "-" + i);
        num_start = datetmp.getDay();
        for (j = 0; j < 6 - num_start; j++) {
          li += "<li></li>";
        }
      }
    }
    datetime1.html(li);
  }
}
function setDateTimeButton(month, year) {
  if (typeof month === 'undefined' || typeof year === 'undefined') {
    var now = new Date();
    var current_month = now.getMonth();
    current_month = parseInt(current_month) + 1;
    current_month = current_month.toString().length < 2 ? '0' + current_month : current_month;
    var current_year = now.getFullYear();
    tmp_start_month = current_month;
    tmp_start_year = current_year;
    CalTime(current_month, current_year, next, prev);
  } else {
    CalTime(month, year, next, prev);
  }
}
function setDateTimeButton1(month, year) {
  if (typeof month === 'undefined' || typeof year === 'undefined') {
    var now = new Date();
    var current_month = now.getMonth();
    current_month = parseInt(current_month) + 1;
    current_month = current_month.toString().length < 2 ? '0' + current_month : current_month;
    var current_year = now.getFullYear();
    tmp_end_month = current_month;
    tmp_end_year = current_year;
    CalTime1(current_month, current_year, next1, prev1);
  } else {
    CalTime1(month, year, next1, prev1);
  }
}
function CalTime(month, year, c_next, c_prev) {
  month = parseInt(month);
  year = parseInt(year);
  if (month > 0 && month <= 12) {
    if (month == 1) {
      c_prev.attr({
        month: 12,
        year: parseInt(year) - 1
      });
      c_next.attr({
        month: parseInt(month) + 1,
        year: year
      });
    } else if (month == 12) {
      c_prev.attr({
        month: 11,
        year: year
      });
      c_next.attr({
        month: 1,
        year: parseInt(year) + 1
      });
    } else {
      c_prev.attr({
        month: month.toString().length < 2 ? '0' + (month - 1) : month - 1,
        year: year
      });
      c_next.attr({
        month: month.toString().length < 2 ? '0' + (month + 1) : month + 1,
        year: year
      });
    }
    var m = month.toString().length < 2 ? '0' + month : month;
    inner.text('' + year + '.' + m + '');
    renderCalendar(month, year);
  }
}
function CalTime1(month, year, c_next, c_prev) {
  month = parseInt(month);
  year = parseInt(year);
  if (month > 0 && month <= 12) {
    if (month == 1) {
      c_prev.attr({
        month: 12,
        year: parseInt(year) - 1
      });
      c_next.attr({
        month: parseInt(month) + 1,
        year: year
      });
    } else if (month == 12) {
      c_prev.attr({
        month: 11,
        year: year
      });
      c_next.attr({
        month: 1,
        year: parseInt(year) + 1
      });
    } else {
      c_prev.attr({
        month: month.toString().length < 2 ? '0' + (month - 1) : month - 1,
        year: year
      });
      c_next.attr({
        month: month.toString().length < 2 ? '0' + (month + 1) : month + 1,
        year: year
      });
    }
    var m = month.toString().length < 2 ? '0' + month : month;
    inner1.text('' + year + '.' + m + '');
    renderCalendar1(month, year);
  }
}
function selectday() {
  $('.cld_body_start .day span').on('click', function () {
    $('.cld_body_start .day span').removeClass('starday');
    $(this).addClass('starday');
    start_date = $(this).attr('data-time');
    start_month = tmp_start_month;
    start_year = tmp_start_year;
    setDateTimeButton(start_month, start_year);
    setDateTimeButton1(end_month, end_year);
    selectday();
  });
  $('.cld_body_end .day span').on('click', function () {
    $('.cld_body_end .day span').removeClass('endday');
    $(this).addClass('endday');
    end_date = $(this).attr('data-time');
    end_month = tmp_end_month;
    end_year = tmp_end_year;
    setDateTimeButton(start_month, start_year);
    setDateTimeButton1(end_month, end_year);
    selectday();
  });
}
function initCalendar() {
  var time = $(inputName).val();
  console.log(time);
  console.log(time);
  if (time != '') {
    console.log('meomeo');
    var res = time.split("-");
    var tmp_start = res[0].split(".");
    var tmp_end = res[1].split(".");
    start_date = tmp_start[2];
    start_month = tmp_start[1];
    start_year = tmp_start[0];
    end_date = tmp_end[2];
    end_month = tmp_end[1];
    end_year = tmp_end[0];
    console.log(start_date);
    console.log(start_month);
    console.log(start_year);
    console.log(end_date);
    console.log(end_month);
    console.log(end_year);
    setDateTimeButton(start_month, start_year);
    setDateTimeButton1(end_month, end_year);
  } else {
    setDateTimeButton();
    setDateTimeButton1();
  }
  prev.click(function (e) {
    e.preventDefault();
    var month = parseInt($(this).attr('month'));
    var year = parseInt($(this).attr('year'));
    setDateTimeButton(month, year);
    tmp_start_month = month;
    tmp_start_year = year;
    selectday();
    console.log('dsdsf');
  });
  next.click(function (e) {
    e.preventDefault();
    var month = parseInt($(this).attr('month'));
    var year = parseInt($(this).attr('year'));
    tmp_start_month = month;
    tmp_start_year = year;
    setDateTimeButton(month, year);
    selectday();
  });
  prev1.click(function (e) {
    e.preventDefault();
    var month = parseInt($(this).attr('month'));
    var year = parseInt($(this).attr('year'));
    tmp_end_month = month;
    tmp_end_year = year;
    setDateTimeButton1(month, year);
    selectday();
  });
  next1.click(function (e) {
    e.preventDefault();
    var month = parseInt($(this).attr('month'));
    var year = parseInt($(this).attr('year'));
    tmp_end_month = month;
    tmp_end_year = year;
    setDateTimeButton1(month, year);
    selectday();
  });
}
var inputName = '#datebooking';
var init = false;
function _init() {
  var time = $(inputName).val();
  if (time != '') {
    var res = time.split("-");
    var tmp_start = res[0].split(".");
    var tmp_end = res[1].split(".");
    start_date = tmp_start[2];
    start_month = tmp_start[1];
    start_year = tmp_start[0];
    end_date = tmp_end[2];
    end_month = tmp_end[1];
    end_year = tmp_end[0];
  } else {
    var tmp_date = new Date();
    start_date = undefined;
    start_month = tmp_date.getMonth() + 1;
    start_year = tmp_date.getFullYear();
    tmp_start_month = undefined;
    tmp_start_year = undefined;
    end_date = undefined;
    end_month = tmp_date.getMonth() + 1;
    end_year = tmp_date.getFullYear();
    tmp_end_month = undefined;
    tmp_end_year = undefined;
  }
  setDateTimeButton(start_month, start_year);
  setDateTimeButton1(end_month, end_year);
}
$(document).ready(function () {
  $('#datebooking').on('click', function () {
    $('#datetimepicker').modal('show');
    inputName = this;
    if (init == false) {
      init = true;
      initCalendar();
      selectday();
    } else {
      _init();
      selectday();
    }
  });
  $('#inputDate2').on('click', function () {
    $('#datetimepicker').modal('show');
    inputName = this;
    if (init == false) {
      init = true;
      initCalendar();
      selectday();
    } else {
      _init();
      selectday();
    }
  });
  $('#apply_time').on('click', function () {
    if (start_date.length < 2) {
      start_date = '0' + start_date;
    }
    if (end_date.length < 2) {
      end_date = '0' + end_date;
    }
    if (start_month.toString().length < 2) {
      start_month = '0' + start_month.toString();
    }
    if (end_month.toString().length < 2) {
      end_month = '0' + end_month.toString();
    }
    $(inputName).val(start_year + '.' + start_month + '.' + start_date + '-' + end_year + '.' + end_month + '.' + end_date);
    $('#datetimepicker').modal('hide');
  });
});